import React, {Component} from 'react';
import {Text, View, Alert,Button, FlatList, TouchableOpacity,StyleSheet} from 'react-native';
import Timer from './Timer';
import {connect} from 'react-redux';
import SessionItem from './sessionItem'
import * as actionTypes from '../types/TimerTypes';



 class Pomodoro extends Component {
         state = {
           visible: false,
           modalNum:-10
         };

         componentDidMount(){
           this.props.getSessions()
         }
         imFinishedRound = () =>
           new Promise(resolve => {
             if (this.props.rounds < 4 && !this.props.finished) {
               Alert.alert(
                 'Round Finished!',
                 'You finished the round successfully',
                 [
                   {
                     text: 'Next Round',
                     onPress: () => {
                       this.props.increaseRounds();
                       resolve();
                     },
                   },
                   {
                     text: 'Take Break',
                     onPress: () => {
                       this.props.increaseBreaks();
                       resolve();
                     },
                   },
                 ],
               );
             } else if (this.props.finished){
               this.props.increaseSessions()
             }
             else {
               this.imFinishedSession(resolve);
              
             }
           });

         imFinishedSession = (resolve) => {
          //  if(!this.props.tookBreak){
              Alert.alert(
                'Congrats!!!',
                'You finished the Session successfully',
                [
                  {
                    text: 'Next Session',
                    onPress: () => {
                      this.props.increaseSessions().then(() => {
                        resolve();
                      });
                    },
                  },
                 
                  !this.props.tookBreak? {
                    text: 'Take Break',
                    onPress: () => {
                      this.props.increaseBreaksSessions().then(() => resolve());
                    },
                  }:null,
                ],
              );
            
         };
       
       
         render() {
           return (
             <View>
               <Timer
                 timestamp={
                   new Date(
                     0,
                     0,
                     0,
                     0,
                     this.props.minutes,
                     this.props.seconds,
                     0,
                   )
                 }
                 finished={this.imFinishedRound}
               />
               <FlatList
                 data={this.props.sessions}
                 keyExtractor={(item, index) => item + index.toString()}
                 renderItem={item => {
                   return (
                     <TouchableOpacity
                       onPress={() => this.setState({modalNum: item.index})}>
                       <View style={styles.listItem}>
                         <Text style={{fontSize: 20, color: '#444'}}>
                           Session {item.index + 1}
                         </Text>
                         <SessionItem
                           rounds={item.item.rounds}
                           skips={item.item.skips}
                           rests={item.item.rests}
                           setVisible={()=>{this.setState({modalNum:-10})}}
                           visible={this.state.modalNum === item.index}
                         />
                       </View>
                     </TouchableOpacity>
                   );
                 }}
               />
               <Button title="Post Sessions" onPress={()=>this.props.postSessions(this.props.sessions)}/>
                 <Text>Total Sessions Rests: {this.props.totalRests}</Text>
             </View>
           );
         }
       }
const mapStateToProps = state => {
  return {
    rounds: state.sessions[state.sessionIndex].rounds,
    skips: state.sessions[state.sessionIndex].skips,
    rests: state.sessions[state.sessionIndex].rests,
    minutes:state.time.min,
    seconds:state.time.sec,
    totalRests:state.sessionRests,
    sessionIndex: state.sessionIndex,
   tookBreak:state.sessionTookBreak,
    rest: state.sessions[state.sessionIndex].rests,
    sessions: state.sessions,
    finished:state.finished
  };
};
const mapDispatchToProps = dispatch => {
  return {
    increaseRounds: () => dispatch({type: actionTypes.TIMER_Next_ROUND}),
    increaseBreaks: () => dispatch({type: actionTypes.TIMER_REST_ROUND}),
    increaseSessions: () =>
      new Promise(resolve => {
        dispatch({type: actionTypes.TIMER_FINISHED_SESSION});
        resolve();
      }),
    increaseBreaksSessions: () =>
      new Promise(resolve => {
        dispatch({type: actionTypes.TIMER_REST_SESSION});
        resolve();
      }),
    getSessions: () => dispatch({type: actionTypes.GET_SESSIONS}),

    postSessions: data =>
      dispatch({type: actionTypes.POST_SESSIONS, payload: data}),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Pomodoro);

const styles = StyleSheet.create({
  listItem:{
    textAlign:'center',
    height:50,
    borderBottomWidth:1,
    borderBottomColor:'#CCC',
    alignItems:'center',
    justifyContent:'center',
    marginTop:10
    
  }
})