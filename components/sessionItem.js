import React from 'react'
import {Modal, Text, View ,StyleSheet,Button} from 'react-native'
 
const sessionItem = (props)=> {
    let {rounds , skips , rests,visible} = props
    return (
      <View style={styles.container}>
        <Modal visible={visible} animationType="fade">
          <Text style={{fontSize: 20}}> Skips {skips} </Text>
          <Text style={{fontSize: 20}}> Rounds {props.rounds} </Text>
          <Text style={{fontSize: 20}}> Rests {rests} </Text>
          <Button title="Go Back" onPress={() => props.setVisible()} />
        </Modal>
      </View>
    );

} 
export default sessionItem
const styles = StyleSheet.create({
    container:{
        flex:1,
        width:300,
        height:200,
        alignItems:'center',
        justifyContent:'center'
    }
})