import * as actionTypes from '../../types/TimerTypes';
const initialState = {
  sessionIndex: 0,
  sessionEnded: false,
  sessionRests: 0,
  sessionTookBreak:false,
  finished:false,
  sessions: [
    {
      rounds:1,
      rests: 0,
      skips: 0,
    },
  ],

  time: {
    min: 25,
    sec: 0,
  },
};
const sessionReducers = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TIMER_TRAIN:
      return {
        ...state,
        sessions: update(state, 'rounds'),
      };

    case actionTypes.TIMER_REST_ROUND:
      return {
        ...state,
        sessions: update(state, 'rests'),
        time: {min: 10, sec: 0},
      };
    case actionTypes.TIMER_REST_SESSION:
      let newRest = state.sessionRests;
      newRest += 1;
      return {
        ...state,
        sessionRests: newRest,
        sessionTookBreak: true,
        time: {min: 25, sec: 0},
      };
    case actionTypes.TIMER_Next_ROUND:
      return {
        ...state,
        time: {min: 25, sec: 0},
        sessions: update(state, 'rounds'),
      };
    case actionTypes.TIMIER_Next_SESSION:
      let newIndex = (state.sessionIndex += 1);
      let newSession = [...state.sessions, {rounds: 1, rests: 0, skips: 0}];

      let timeObj = {min: 25, sec: 0};
      return {
        ...state,
        sessionEnded: true,
        time: timeObj,
        sessionIndex: newIndex,
        sessions: newSession,
      };
    case actionTypes.ADD_DATA:
      return {
        ...state,
        sessions: action.payload,
      };
 
    default:
      return {
        ...state,
      };
  }
};
let update = (state,value) =>{
let newOb = [...state.sessions]
let tempObj = newOb[state.sessionIndex]
if(value === "rounds"){
  tempObj.skips += 1
}
tempObj[value]+=1
newOb[state.sessionIndex] = tempObj
return newOb
}
export default sessionReducers;
