import {takeLatest,put,call,takeEvery} from 'redux-saga/effects'
import * as actionTypes from '../../types/TimerTypes'
import sessionReducers from '../reducers/sessionReducer'
import axios from '../../axios-requests'
 function* get_sessions(){
    try {
        const res = yield axios.get('get_sessions')
        yield put({type:actionTypes.ADD_DATA,payload:res.data})
    } catch (err) {
        
    }
    
   
}
 function* post_sessions(data) {
   try {
     const res = yield axios.post('post_sessions',data.payload);
   } catch (err) {}
 }
function *next_Session(){
    yield put({type:actionTypes.TIMIER_Next_SESSION})
}

function* start(){
    yield takeLatest(actionTypes.GET_SESSIONS,get_sessions)
    yield takeLatest(actionTypes.POST_SESSIONS,post_sessions)
    yield takeEvery(actionTypes.TIMER_FINISHED_SESSION,next_Session)
}
export default start